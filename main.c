#include <stdio.h>
#include <stdlib.h>

union bitToByte
{
	struct
	{
		char b0 : 1;
		char b1 : 1;
		char b2 : 1;
		char b3 : 1;
		char b4 : 1;
		char b5 : 1;
		char b6 : 1;
		char b7 : 1;
	} bits;
	char byte;
} mbtb, kbtb, cbtb;

int main()
{
	FILE *mes = fopen("mes.txt", "r");
	FILE *key = fopen("key.txt", "r");
	FILE *crp = fopen("crp.txt", "w");
	int mesChar = 0;
	int keyChar = 0;
	int crpChar = 0;

	if(mes == NULL || key == NULL || crp == NULL)
	{
		printf("ERROR: Folowing files can't open:\n");
		printf("%s%s%s",
			(mes == NULL) ? "mes.txt\n" : "",
			(key == NULL) ? "key.txt\n" : "",
			(crp == NULL) ? "crp.txt\n" : "");
		return 1;
	}
	while((mesChar = getc(mes)) != EOF)
	{
		mbtb.byte = mesChar;
		kbtb.byte = keyChar = getc(key);
		if(keyChar == EOF)
		{
			printf("ERROR: Key must be bigger then the message.\n");
			return 2;
		}
		cbtb.bits.b0 = mbtb.bits.b0 ^ kbtb.bits.b0;
		cbtb.bits.b1 = mbtb.bits.b1 ^ kbtb.bits.b1;
		cbtb.bits.b2 = mbtb.bits.b2 ^ kbtb.bits.b2;
		cbtb.bits.b3 = mbtb.bits.b3 ^ kbtb.bits.b3;
		cbtb.bits.b4 = mbtb.bits.b4 ^ kbtb.bits.b4;
		cbtb.bits.b5 = mbtb.bits.b5 ^ kbtb.bits.b5;
		cbtb.bits.b6 = mbtb.bits.b6 ^ kbtb.bits.b6;
		cbtb.bits.b7 = mbtb.bits.b7 ^ kbtb.bits.b7;
		putc(cbtb.byte, crp);
	}
	return 0;
}
